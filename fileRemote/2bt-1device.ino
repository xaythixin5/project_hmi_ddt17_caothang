#include <SoftwareSerial.h>

SoftwareSerial mySerial(10, 11); // RX, TX
byte thietBi1 = 13;
byte thietBi2 = 8;
void setup()
{
    pinMode(thietBi1, OUTPUT);
    pinMode(thietBi2, OUTPUT);

    Serial.begin(115200);
    while (!Serial)
    {
    }
    mySerial.begin(9600);
}

void loop()
{
    byte value = 0xff;
    if (mySerial.available())
    {
        value = mySerial.read();
        Serial.println(value, HEX);
    }
    switch (value)
    {
    case 0x11:
        digitalWrite(thietBi1, HIGH);
        break;
    case 0x10:
        digitalWrite(thietBi1, LOW);
        break;
    case 0x01:
        digitalWrite(thietBi2, HIGH);
        break;
    case 0x00:
        digitalWrite(thietBi2, LOW);
        break;
    default:
        break;
    }
    // if (Serial.available())
    // {
    //     mySerial.write(Serial.read());
    // }
}
