// cai dat thu vien DHT-sensor-library
#include <SoftwareSerial.h>
#include "DHT.h"
#define DHTPIN 12
#define DHTTYPE DHT11
SoftwareSerial mySerial(10, 11); // RX, TX
byte thietBi1 = A0;
byte thietBi2 = A1;
DHT dht(DHTPIN, DHTTYPE);
void setup()
{
  pinMode(thietBi1, OUTPUT);
  pinMode(thietBi2, OUTPUT);

  Serial.begin(115200);
  while (!Serial)
  {
  }
  mySerial.begin(9600);
  delay(1000);
  dht.begin();
}

void loop()
{
  byte value = 0xff;
  while (mySerial.available())
  {
    value = mySerial.read();
    Serial.println(value, HEX);
    switch (value)
    {
      case 0x11:
        digitalWrite(thietBi1, HIGH);
        break;
      case 0x10:
        digitalWrite(thietBi1, LOW);
        break;
      case 0x01:
        digitalWrite(thietBi2, HIGH);
        break;
      case 0x00:
        digitalWrite(thietBi2, LOW);
        break;
      default:
        break;
    }
  }
  float doAm = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float nhietDo = dht.readTemperature();
  int randNumber = random(10, 9999);
  String sendTo1 = "x0.val=";
  sendTo1 += int(nhietDo *100);
  String sendTo2 = "x1.val=";
  sendTo2 += int(doAm);
  mySerial.print(sendTo1);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(500);
  mySerial.print(sendTo2);
  mySerial.write(0xff);
  mySerial.write(0xff);
  mySerial.write(0xff);
  delay(500);
  Serial.print("do Am: ");
  Serial.println(doAm);
  Serial.println("nhiet do: ");
  Serial.println(nhietDo);
}
